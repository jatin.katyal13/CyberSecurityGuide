package com.guide.safety.cybersafetyguide;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;

public class welcome extends Activity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        try {
            setContentView(R.layout.activity_welcome);
            ((Button)findViewById(R.id.log_in)).setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    Intent intent = new Intent (welcome.this, login.class);
                    startActivity(intent);
                }
            });

            ((Button)findViewById(R.id.sign_up)).setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    Intent intent = new Intent (welcome.this, sign_in.class);
                    startActivity(intent);
                }
            });
        } catch (Exception e){
            //
            //Log.e("jatin", e.toString());
            //e.printStackTrace();
            Intent intent = new Intent(this, sign_in_duplicate.class);
            startActivity(intent);
        }
    }
}
