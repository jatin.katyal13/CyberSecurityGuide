package com.guide.safety.cybersafetyguide;


import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.os.Bundle;
import android.os.Handler;
import android.support.design.widget.FloatingActionButton;
import android.support.v4.app.Fragment;
import android.support.v7.app.AlertDialog;
import android.support.v7.view.ContextThemeWrapper;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.net.URLEncoder;

public class volunteer_fragment extends Fragment {

    static Context context;

    public volunteer_fragment() {
        // Required empty public constructor
    }

    ProgressDialog progressDialog;

    public void add_context(Context context){
        this.context = context;
    }


    @Override
    public View onCreateView(LayoutInflater inflater, final ViewGroup container,
                             Bundle savedInstanceState) {
        final View rootView = inflater.inflate(R.layout.content_volunteer, container, false);

        try {
            InputStream in = context.openFileInput("secure.db");
            int c;
            String json = "";
            while ((c = in.read()) != -1){
                json += (char)c;
            }
            JSONObject object = new JSONObject(json);
            ((TextView)rootView.findViewById(R.id.name)).setText(object.getString("name"));
            ((TextView)rootView.findViewById(R.id.email)).setText(object.getString("email"));
            ((TextView)rootView.findViewById(R.id.phone)).setText(object.getString("phone"));

        } catch (Exception e){

        }

        FloatingActionButton button = (FloatingActionButton)rootView.findViewById(R.id.fab);
        button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                final String name = ((EditText)rootView.findViewById(R.id.name)).getText().toString();
                final String email = ((EditText)rootView.findViewById(R.id.email)).getText().toString();
                final String phone = ((EditText)rootView.findViewById(R.id.phone)).getText().toString();
                final String mess = ((EditText)rootView.findViewById(R.id.message)).getText().toString();

                if (name.trim().equals("")){
                    Toast.makeText(context, "Please fill in your Name", Toast.LENGTH_SHORT).show();
                } else if (email.trim().equals("") || (!email.contains("@") || !(email.indexOf("@") == email.lastIndexOf("@")) )){
                    Toast.makeText(context, "Please fill in your EMail address correctly", Toast.LENGTH_SHORT).show();
                } else if (phone.trim().equals("")){
                    Toast.makeText(context, "Please fill in your Phone Number", Toast.LENGTH_SHORT).show();
                } else if (!(phone.trim().length() == 10) && !(phone.trim().length() == 8)){
                    Toast.makeText(context, "Please fill in your Phone Number Correctly without STD code", Toast.LENGTH_SHORT).show();
                } else if (mess.trim().equals("")){
                    Toast.makeText(context, "Please write a message for us too !", Toast.LENGTH_SHORT).show();
                } else {
                    final String subject = "Volunteer request";
                    String message = "";

                    message += "<b>Name: </b>" + name + "\r\n";
                    message += "<b>Phone: </b>" + phone + "\r\n";
                    message += "<b>EMail: </b>" + email + "\r\n";
                    message += "<b>Message: </b>" + mess;

                    final String finalMessage = message;
                    final String finalEmail = email;

                    new Thread(new Runnable() {
                        @Override
                        public void run() {
                            new Handler(context.getMainLooper()).post(new Runnable() {
                                @Override
                                public void run() {
                                    progressDialog = ProgressDialog.show(context, "", "Sending", true);
                                }
                            });
                            try{
                                lib.executePost("http://www.abboniss.com/test/sendMail",
                                        "code = " + URLEncoder.encode("securepass@csao2016", "UTF-8") +
                                        "&to = " + URLEncoder.encode("volunteer.csao@gmail.com", "UTF-8") +
                                        "&subject = " + URLEncoder.encode(subject, "UTF-8") +
                                        "&body = " + URLEncoder.encode(finalMessage, "UTF-8") +
                                        "&from = " + URLEncoder.encode(finalEmail, "UTF-8")
                                    );

                                new Handler(context.getMainLooper()).post(new Runnable() {
                                    @Override
                                    public void run() {
                                        try {
                                            FileInputStream file = context.openFileInput("history.txt");
                                            String jsonText="";
                                            int c;
                                            while ((c=file.read()) != -1){
                                                jsonText += (char)c;
                                            }
                                            JSONArray arr = new JSONArray(jsonText);
                                            arr.put(0, new JSONObject("{\"heading\":\"Volunteer Request\",\"name\":\""+name+"\", \"email\":\""+email+"\", \"phone\":\""+phone+"\", \"message\":\""+mess+"\"}"));
                                            file.close();
                                            context.openFileOutput("history.txt", Context.MODE_PRIVATE).write(arr.toString().getBytes());
                                            Log.d("jatin", "FIle created with content: "+arr.toString());
                                        } catch (JSONException e){
                                            Log.d("jatin", "Json Exception: "+e.toString());
                                            JSONArray arr = new JSONArray();
                                            try {
                                                arr.put(new JSONObject("{\"heading\":\"Volunteer Request\",\"name\":\""+name+"\", \"email\":\""+email+"\", \"phone\":\""+phone+"\", \"message\":\""+mess+"\"}"));
                                                context.openFileOutput("history.txt", Context.MODE_PRIVATE).write(arr.toString().getBytes());
                                                Log.d("jatin", "Content of file: "+arr.toString());
                                            } catch (JSONException ex){
                                                Log.d("jatin", ex.toString());
                                            } catch (IOException ex){
                                                Log.d("jatin", ex.toString());
                                            }
                                        } catch (Exception e){

                                        }
                                        new Handler(context.getMainLooper()).post(new Runnable() {
                                            @Override
                                            public void run() {
                                                progressDialog.cancel();
                                            }
                                        });
                                        AlertDialog.Builder builder = new AlertDialog.Builder(new ContextThemeWrapper(context, R.style.myDialog));
                                        builder.setTitle("Request Sent");
                                        builder.setMessage("Thanks for showing interest with our Team. We will contact you soon !");
                                        builder.setPositiveButton("OK", new DialogInterface.OnClickListener() {
                                            @Override
                                            public void onClick(DialogInterface dialog, int which) {

                                            }
                                        });
                                        AlertDialog alert = builder.create();
                                        alert.show();
                                    }
                                });
                            } catch (Exception e){
                                new Handler(context.getMainLooper()).post(new Runnable() {
                                    @Override
                                    public void run() {
                                        Toast.makeText(context, "Error Occured !", Toast.LENGTH_SHORT).show();
                                    }
                                });
                            }
                            progressDialog.cancel();

                        }
                    }).start();
                }
            }
        });

        return rootView;
    }

}
