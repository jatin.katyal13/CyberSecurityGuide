package com.guide.safety.cybersafetyguide;

import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.os.Bundle;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import org.json.JSONArray;
import org.json.JSONObject;

import java.io.BufferedInputStream;
import java.io.InputStream;
import java.io.OutputStream;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.ArrayList;
import java.util.List;

public class threats extends AppCompatActivity {

    List<threat_data> list = new ArrayList<>();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_threats);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        final SwipeRefreshLayout refreshLayout = (SwipeRefreshLayout)findViewById(R.id.swipe_refresh_layout);


        final ListView listView = (ListView)findViewById(R.id.list);

        new Thread(new Runnable() {
            @Override
            public void run() {
                runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        refreshLayout.setRefreshing(true);
                    }
                });

                list = getJson();

                runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        listView.setAdapter(new ListAdapter(getApplicationContext(), list));
                        refreshLayout.setRefreshing(false);
                    }
                });
            }
        }).start();

        listView.setAdapter(new ListAdapter(getApplicationContext(), list));
        listView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                Intent intent = new Intent(threats.this, threat_display.class);
                intent.putExtra("heading", list.get(position).heading);
                intent.putExtra("description", list.get(position).description);
                intent.putExtra("threats", list.get(position).threats);
                startActivity(intent);
            }
        });



        refreshLayout.setColorSchemeColors(Color.BLUE, Color.RED, Color.YELLOW, Color.GREEN);
        refreshLayout.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                new Thread(){
                    @Override
                    public void run() {
                        super.run();
                        try {
                            List<threat_data> rList = new ArrayList<threat_data>();
                            URL url = new URL("http://www.abboniss.com/test/threats_get.php");
                            HttpURLConnection urlConnection = (HttpURLConnection) url.openConnection();
                            InputStream in = new BufferedInputStream((urlConnection.getInputStream()));
                            int c;
                            String jsonText = "";
                            while ((c = in.read()) != -1){
                                jsonText += String.valueOf((char)c);
                            }
                            OutputStream file = openFileOutput("threats.txt", MODE_PRIVATE);
                            file.write(jsonText.getBytes());
                            JSONObject object = new JSONObject(jsonText);
                            JSONArray array = object.getJSONArray("info");
                            for (int i=0; i<array.length(); i++){
                                JSONObject obj = array.getJSONObject(i);
                                rList.add(new threat_data(obj.getString("heading"), obj.getString("what_it_is"), obj.getString("what_it_can_do")));
                            }
                            list = rList;
                            runOnUiThread(new Runnable() {
                                @Override
                                public void run() {
                                    try {
                                        listView.setAdapter(new ListAdapter(getApplicationContext(), list));
                                    } catch (Exception e){
                                        Toast.makeText(threats.this, "Error occured !", Toast.LENGTH_SHORT).show();
                                    }
                                    refreshLayout.setRefreshing(false);
                                }
                            });
                        }
                        catch (Exception e){
                            runOnUiThread(new Runnable() {
                                @Override
                                public void run() {
                                    Toast.makeText(threats.this, "Unable to refresh", Toast.LENGTH_SHORT).show();
                                    refreshLayout.setRefreshing(false);
                                }
                            });
                        }
                    }
                }.start();
            }
        });

    }

    private class threat_data {
        String heading;
        String description;
        String threats;
        public threat_data(String h, String d, String t){
            heading = h;
            description = d;
            threats = t;
        }
    }

    private List<threat_data> getJson(){
        List<threat_data> empty = new ArrayList<>();

        try {
            List<threat_data> list = new ArrayList<>();
            InputStream file = openFileInput("threats.txt");
            String jsonText = "";
            int c;
            while ((c = file.read()) != -1){
                jsonText += (char)c;
            }
            JSONObject object = new JSONObject(jsonText);
            JSONArray array = object.getJSONArray("info");
            for (int i=0; i<array.length(); i++){
                JSONObject obj = ((JSONObject) array.get(i));
                list.add(new threat_data(obj.getString("heading"), obj.getString("what_it_is"), obj.getString("what_it_can_do")));
            }
            return list;
        } catch (Exception e){
            try {
                List<threat_data> list = new ArrayList<>();
                String jsonText = "{\"info\":[{\"heading\":\"Botnets\",\"what_it_is\":\"A botnet (also known as a zombie army) is a number of Internet computers that, although their owners are unaware of it, have been set up to forward transmissions (including spam or viruses) to other computers on the Internet.\",\"what_it_can_do\":\"Send spam emails with viruses attached. Spread all types of malware.\"},{\"heading\":\"Hacking and cracking\",\"what_it_is\":\"Hacking is a particular type of illegal activity which endangers the privacy and security of important on line information.Hacking challenges the utility of the mechanisms which are used to secure a communication network. It is important here to note that although hacking is used interchangeably with cracking, both are different. Both the activities are carried on with different objectives. In hacking, the attempt is to create something while in cracking the primary motive is to destroy something.\",\"what_it_can_do\":\"Find weaknesses (or pre-existing bugs) in your security settings and exploit them in order to access your information.Install a Trojan horse, providing a back door for hackers to enter and search for your information.\"},{\"heading\":\"Malware\",\"what_it_is\":\"Malware is an umbrella term used to refer to a variety of forms of hostile or intrusive software, including computer viruses, worms, trojan horses,ransomware, spyware, adware, scareware, and other malicious programs. It can take the form of executable code, scripts, active content, and other software.\",\"what_it_can_do\":\"1. It can format the hard drive of your computer causing you to lose all your information. 2. Alter ,delete or steal files. 3.It can send hoax emails on your behalf. 4.Take control of your computer and all the software running on it.\"},{\"heading\":\"Pharming\",\"what_it_is\":\"Pharming is a cyber attack intended to redirect a websites traffic to another, fake site. Pharming can be conducted either by changing the hosts file on a victim's computer or by exploitation of a vulnerability in DNS server software.\",\"what_it_can_do\":\"Convince you that the site is real and legitimate by spoofing or looking almost identical to the actual site down to the smallest details. You may enter your personal information and unknowingly give it to someone with malicious intent.\"},{\"heading\":\"Phishing\",\"what_it_is\":\"Fake emails, text messages and websites created to look like they are from authentic companies.They are sent by criminals to steal personal and financial information from you. This is also known as spoofing.\",\"what_it_can_do\":\"1. It may trick you into giving them information by asking you to update, validate or confirm your account. It is often presented in a manner than seems official and intimidating. 2. It provides cyber criminals with your username and passwords so that they can access your accounts.\"},{\"heading\":\"Ransomware\",\"what_it_is\":\"Ransomware is a type of malware that restricts access to your computer or your files and displays a message that demands payment in order for the restriction to be removed.\",\"what_it_can_do\":\"There are two common types of ransomware: 1.Lockscreen ransomware: displays an image that prevents you from accessing your computer 2. Encryption ransomware: encrypts files on your systems hard drive and sometimes on shared network drives, USB drives, external hard drives, and even some cloud storage drives, preventing you from opening them.\"},{\"heading\":\"Distributed denial-of- service (DDoS) attack\",\"what_it_is\":\"A distributed denial-of-service (DDOS) attack or DDOS attack is when a malicious user gets a network of zombie computers to sabotage a specific website or server. DDOS is a type of DOS attack where multiple compromised systems, which are often infected with a Trojan, are used to target a single system causing Denial of Service (DoS) attack.\",\"what_it_can_do\":\"The most common and obvious type of DDoS attack occurs when an attacker floods a network with useless information. When you type a URL into your browser, you are sending a request to that site's computer server to view the page. The server can only process a certain number of requests at once. If an attacker overloads the server with requests, it can't process yours. The flood of incoming messages to the target system essentially forces it to shut down, thereby denying access to legitimate users.\"},{\"heading\":\"Spam\",\"what_it_is\":\"The mass distribution of unsolicited messages, advertising or pornography to addresses which can be easily found on the Internet through things like social networking sites, company websites and personal blogs.\",\"what_it_can_do\":\"It can phish for your information by tricking you into following links or entering details with too- good-to- be-true offers and promotions.It provide a vehicle for malware, scams, fraud and threats to your privacy.\"},{\"heading\":\"Spyware\",\"what_it_is\":\"Software that collect personal information about you without your knowledge. These are difficult to remove and can infect your computer with viruses.\",\"what_it_can_do\":\"1.Collect information about you without you knowing about it and give it to third parties. 2.Send your usernames, passwords, surfing habits, list of applications you have downloaded, settings, and even the version of your operating system to third parties. 3.Take you to unwanted sites or inundate you with uncontrollable pop-up ads.\"},{\"heading\":\"Trojan Horses\",\"what_it_is\":\"A malicious program that is disguised as, or embedded within, legitimate software. It is an executable file that will install itself and run automatically once it is downloaded.\",\"what_it_can_do\":\"1.Delete your files. 2.Watch you through your web cam. 3. Log your keystrokes (such as a credit card number you entered in an online purchase).\"},{\"heading\":\"Viruses\",\"what_it_is\":\"Malicious computer programs that are often sent as an email attachment or a download with the intent of infecting your computer, as well as the computers of everyone in your contact list.\",\"what_it_can_do\":\"1. Send spam. 2. Provide criminals with access to your computer and contact lists. 3.Hijack your web browser. 4.Disable your security settings. 5.When a program is running, the virus attached to it could infiltrate your hard drive and also spread to USB keys and external hard drives.\"},{\"heading\":\"Wi-Fi Eavesdropping\",\"what_it_is\":\"WiFi eavesdropping is another method used by cyber criminals to capture personal information. Virtual listening in on information that`s shared over an unsecure (not encrypted) WiFi network.\",\"what_it_can_do\":\"Steal your personal information including logins and passwords.\"},{\"heading\":\"Worms\",\"what_it_is\":\"A worm, unlike a virus, goes to work on its own without attaching itself to files or programs. It lives in your computer memory, doesn't damage or alter the hard drive and propagates by sending itself to other computers in a network.\",\"what_it_can_do\":\"1.Spread to everyone in your contact list. 2.Cause a tremendous amount of damage by shutting down parts of the Internet, wreaking havoc on an internal network and costing companies enormous amounts of lost revenue.\"},{\"heading\":\"Online Defamation\",\"what_it_is\":\"An unprivileged false statement of fact which tends to harm the reputation of a person or company. This is a catch-all term for both libel and slander.\",\"what_it_can_do\":\"1.A false statement of fact must harm your reputation. 2. A false statement of fact causing harm must be made without adequate due diligence into the truthfulness of the statement. 3. A Facebook status update about a negative employment incident may receive dozens of 'likes' and comments, but harms a company in long run. \"},{\"heading\":\"Cyber Stalking\",\"what_it_is\":\"The repeated use of electronic communications to harass or frighten someone, for example by sending threatening emails. \",\"what_it_can_do\":\"Cyberstalking can be terribly frightening. It can destroy friendships, credit, careers, self-image, and confidence. Ultimately it can lead the victim into far greater physical danger when combined with real-world stalking. \"},{\"heading\":\"Cyber Bullying\",\"what_it_is\":\"cyberbullying is when a child or teen becomes a target of actions by others – using computers, cellphones or other devices – that are intended to embarrass, humiliate, torment, threaten or harass. It can start at the early age but the majority of cyberbullying takes place in the teenage years. \",\"what_it_can_do\":\"There are two kinds of cyberbullying, direct attacks (messages sent to your kids directly) and cyberbullying by proxy (using others to help cyberbully the victim, either with or without the accomplice's knowledge). Because cyberbullying by proxy often gets adults involved in the harassment, it is much more dangerous. \"}]}"; // some default json encoded string in case the file is not found
                JSONObject object = new JSONObject(jsonText);
                JSONArray array = object.getJSONArray("info");
                for (int i=0; i<array.length(); i++){
                    JSONObject obj = ((JSONObject) array.get(i));
                    list.add(new threat_data(obj.getString("heading"), obj.getString("what_it_is"), obj.getString("what_it_can_do")));
                }
                return list;
            } catch (Exception ex){
                Toast.makeText(threats.this, "Error Occured", Toast.LENGTH_SHORT).show();
            }
        }
        return empty;
    }

    private class ListAdapter extends ArrayAdapter<threat_data> {

        private List<threat_data> list = new ArrayList<>();

        public ListAdapter(Context context, List<threat_data> list) {
            super(context, R.layout.threats_card_item, list);
            this.list = list;
        }

        @Override
        public View getView(int position, View convertView, ViewGroup parent) {
            View rootView = convertView;

            if (rootView == null){
                rootView = getLayoutInflater().inflate(R.layout.threats_card_item, parent, false);
            }

            ((TextView)rootView.findViewById(R.id.tv_text)).setText(list.get(position).heading);

            return rootView;
        }
    }

}
