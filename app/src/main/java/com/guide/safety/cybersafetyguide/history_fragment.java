package com.guide.safety.cybersafetyguide;

import android.content.Context;
import android.net.Uri;
import android.os.Bundle;
import android.os.Handler;
import android.support.v4.app.Fragment;
import android.support.v4.widget.SwipeRefreshLayout;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

public class history_fragment extends Fragment {

    Context context;
    LayoutInflater inflater;

    public history_fragment() {
        // Required empty public constructor
    }

    public void add_context(Context context){
        this.context = context;
    }

    private class ListItem {
        String Heading;
        String Data;
        public ListItem(String h, String d){
            Heading = h;
            Data = d;
        }
    }

    private class HistoryItem {
        String Heading;
        List<ListItem> list;
        public HistoryItem(String h, List<ListItem> l){
            Heading = h;
            list = l;
        }
    }

    private class ParentAdapter extends ArrayAdapter<HistoryItem>{

        List<HistoryItem> list;

        public ParentAdapter(List<HistoryItem> list) {
            super(context, R.layout.history_card_item, list);
            this.list = list;
        }

        @Override
        public View getView(int position, View convertView, ViewGroup parent) {
            View rootView = inflater.inflate(R.layout.history_card_item, parent, false);

            ((TextView)rootView.findViewById(R.id.heading)).setText(list.get(position).Heading);
            ((ListView)rootView.findViewById(R.id.list)).setAdapter(new ChildAdapter(list.get(position).list));
            Log.d("jatin", "adding main card");

            return rootView;
        }

        private class ChildAdapter extends ArrayAdapter<ListItem> {

            List<ListItem> list;

            @Override
            public View getView(int position, View convertView, ViewGroup parent) {
                View rootView = inflater.inflate(R.layout.history_list_item, parent, false);

                ((TextView)rootView.findViewById(R.id.header)).setText(list.get(position).Heading);
                ((TextView)rootView.findViewById(R.id.data)).setText(list.get(position).Data);
                Log.d("jatin", "adding listx");
                return rootView;
            }

            public ChildAdapter(List<ListItem> list){
                super(context, R.layout.history_list_item, list);
                this.list = list;
            }
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        this.inflater = inflater;
        final View rootView = inflater.inflate(R.layout.content_history_fragment, container, false);

        final SwipeRefreshLayout refreshLayout = (SwipeRefreshLayout)rootView.findViewById(R.id.refresh_layout);
        refreshLayout.setActivated(false);
        refreshLayout.setRefreshing(true);
        new Thread(new Runnable() {
            @Override
            public void run() {
                Log.d("jatin", "Trying");
                try {
                    final List<HistoryItem> list = new ArrayList<>();
                    FileInputStream file = context.openFileInput("history.txt");
                    int c;
                    String jsonText = "";
                    while ((c=file.read()) != -1){
                        jsonText += (char)c;
                    }
                    JSONArray obj = new JSONArray(jsonText);
                    for (int i=0; i<obj.length(); i++){
                        JSONObject temp = obj.getJSONObject(i);
                        Iterator<String> iterator = temp.keys();
                        List<ListItem> list1 = new ArrayList<>();
                        String heading = temp.getString(iterator.next());
                        while (iterator.hasNext()){
                            String key = iterator.next();
                            list1.add(new ListItem(key, temp.getString(key)));
                            Log.d("jatin", key);
                            Log.d("jatin", temp.getString(key));
                        }
                        list.add(new HistoryItem(heading, list1));
                    }
                    new Handler(context.getMainLooper()).post(new Runnable() {
                        @Override
                        public void run() {
                            ((ListView)rootView.findViewById(R.id.list)).setAdapter(new ParentAdapter(list));
                        }
                    });
                    Log.d("jatin", "Done");

                } catch (IOException e){
                    Log.d("jatin", "File Not Found: "+e.toString());
                    try {
                        FileOutputStream file = context.openFileOutput("history.txt", Context.MODE_PRIVATE);
                        file.write("".getBytes());
                        Log.d("jatin", "Made a file");
                    } catch (Exception ex){
                        new Handler(context.getMainLooper()).post(new Runnable() {
                            @Override
                            public void run() {
                                Toast.makeText(context, "Error Occured", Toast.LENGTH_SHORT).show();
                            }
                        });
                        Log.d("jatin", "Still a error: "+ex.toString());
                    }
                } catch (JSONException e){
                    Log.d("jatin", "JSON Exception");
                }
                new Handler(context.getMainLooper()).post(new Runnable() {
                    @Override
                    public void run() {
                        refreshLayout.setRefreshing(false);
                    }
                });
            }
        }).start();

        return rootView;
    }
}
