package com.guide.safety.cybersafetyguide;

import android.content.Context;
import android.graphics.Color;
import android.net.Uri;
import android.os.Bundle;
import android.os.Handler;
import android.support.v4.app.Fragment;
import android.support.v4.widget.SwipeRefreshLayout;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import org.json.JSONArray;
import org.json.JSONObject;

import java.io.BufferedInputStream;
import java.io.InputStream;
import java.io.OutputStream;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.ArrayList;
import java.util.List;

public class case_studies_fragment extends Fragment {

    private static Context context;
    private LayoutInflater inflater;

    public case_studies_fragment() {
        // Required empty public constructor
    }

    public void add_context(Context context){
        this.context = context;
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        this.inflater = inflater;
        final View rootView = inflater.inflate(R.layout.content_case_studies, container, false);

        List<CaseStudy> list = getJson();

        ((ListView)rootView.findViewById(R.id.list)).setAdapter(new ListAdapter(context, list));

        final SwipeRefreshLayout refresh_layout = (SwipeRefreshLayout)rootView.findViewById(R.id.refresh_layout);
        refresh_layout.setColorSchemeColors(Color.BLUE, Color.RED, Color.YELLOW, Color.GREEN);
        refresh_layout.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                new Thread(){
                    @Override
                    public void run() {
                        super.run();
                        boolean done = false;
                        final List<CaseStudy> list = new ArrayList<CaseStudy>();
                        try {
                            URL url = new URL("http://www.abboniss.com/test/case_studies_get.php");
                            HttpURLConnection urlConnection = (HttpURLConnection) url.openConnection();
                            InputStream in = new BufferedInputStream((urlConnection.getInputStream()));
                            int c;
                            String jsonText = "";
                            while ((c = in.read()) != -1){
                                jsonText += String.valueOf((char)c);
                            }
                            OutputStream file = context.openFileOutput("json_case_studies.txt", context.MODE_PRIVATE);
                            file.write(jsonText.getBytes());
                            JSONObject obj = new JSONObject(jsonText);
                            JSONArray net = obj.getJSONArray("info");
                            for (int i=0; i<net.length(); i++){
                                list.add(new CaseStudy(net.getJSONObject(i).getString("heading"), net.getJSONObject(i).getString("desc")));
                            }
                            done = true;
                        }
                        catch (Exception e){
                            Log.e("jatin", e.toString());
                            done = false;
                        }
                        final boolean finalDone = done;
                        new Handler(context.getMainLooper()).post(new Runnable() {
                            @Override
                            public void run() {
                                if (finalDone){
                                    ArrayAdapter<CaseStudy> adapter = new ListAdapter(context,list);
                                    ListView listView = (ListView) rootView.findViewById(R.id.list);
                                    listView.setAdapter(adapter);
                                }
                                else {
                                    Toast.makeText(context, "Unable to refresh !", Toast.LENGTH_SHORT).show();
                                }
                                refresh_layout.setRefreshing(false);
                            }
                        });
                    }
                }.start();
            }
        });

        return rootView;
    }

    private List<CaseStudy> getJson(){
        List<CaseStudy> empty_list = new ArrayList<>();
        try {
            List<CaseStudy> list = new ArrayList<CaseStudy>();
            InputStream file = context.openFileInput("json_case_studies.txt");
            String jsonText = "";
            int c;
            while ((c = file.read()) != -1){
                jsonText += String.valueOf((char)c);
            }
            JSONObject obj = new JSONObject(jsonText);
            JSONArray net = obj.getJSONArray("info");
            for (int i=0; i<net.length(); i++){
                list.add(new CaseStudy(net.getJSONObject(i).getString("heading"), net.getJSONObject(i).getString("desc")));
            }
            return list;
        } catch (Exception ex){
            try {
                List<CaseStudy> list = new ArrayList<CaseStudy>();
                String jsonText = "{\"info\":[{\"heading\":\"Cyber Security \", \"desc\":\"Cyber security is very important when the use of computers, e mails and mobile phones is increasing. IT infrastructure is secure with corporates, since it is programmed internally by them. The infrastructure of use of mobile/smart phones is vulnerable to more cyber assess and needs more security. New mode of threat is ransomware. Here, attackers lure the users to download an encryption programme for encrypting their files. For return of files for decrypting, they ask for money. Users should be aware about this threat. Chris Young (Senior VP, Intel Security). Times of India (Ahmedabad) 5 April, 2016 \"},{\"heading\":\"Cyber Crimes News\", \"desc\":\"With the increase in use of information technology/internet, there is a big increase in Cyber-crimes. This ranges from theft of bank account PIN to theft of personal information. On account of such cyber-crimes, annually there is a loss of Rs. 30,000 crores in the world. Like Edward Snowden, there are so many hackers in the world. As per Julius Assange, Internet is the biggest medium of spying. Over and above the spying, it is a platform of economic/financial cheating. It is advisable to limit uploading personal information/photographs on social sites. These views were expressed by Triveni Singh (Director, Special Task Force, Cyber Cell) at Chartered Accountants’ Conference at Rajkot. Gujarat Samachar (20 Dec., 2015) \"},{\"heading\":\"A Cyber Crime Case\", \"desc\":\"Cyber Crime Branch, Ahmedabad arrested a person from Shirdi, for attempting to cheat an Ahmedabad based company of Rs.73,41,000 owed to it by another company. This was done by creating an e mail account similar to that of the Ahmedabad based company, and sent an e mail asking the other company to deposit its dues to a bank account mentioned in the e mail. However, that company was suspicious and informed the police. Then the police traced and arrested the miscreant from Shirdi. Times of India, Ahmedabad (3 Dec., 2015) \"},{\"heading\":\"Internet Law\", \"desc\":\"At present, E Commerce is operating in multiple sectors. However, some visible legal skirmishes could be: (1) Existing conventional business faces big challenge on account of E Commerce. They already approach the courts with complaints. In future more and serious complaints will emerge. (2) In view of new way of doing the business, i.e., E Commerce, Competition Law has to be amended. (3) The relevance of Sec. 79 of Information Technology Act, 2000 (responsibility of network service providers for content) has to be refined. Source: Internet Law (Ajit Balakrishnan) Business Standard. 20 Oct., 2015. \"},{\"heading\":\"Blocking of offensive sites\", \"desc\":\"The Government of India has ordered the telecom operators and internet service providers to block 857 porn sites. The order is issued under the provisions of Information Technology Act, 2000 and Article 19 (2) of The Constitution of India. There is a view that this can be assessed in privacy. The government is of the opinion that such things should not be viewed in public places like cyber cafes. The Times of India, Ahmedabad. (3 August, 2015) \"}]}";
                JSONObject obj = new JSONObject(jsonText);
                JSONArray net = obj.getJSONArray("info");
                for (int i=0; i<net.length(); i++){
                    list.add(new CaseStudy(net.getJSONObject(i).getString("heading"), net.getJSONObject(i).getString("desc")));
                }
                OutputStream out = context.openFileOutput("json_case_studies.txt", context.MODE_PRIVATE);
                out.write(jsonText.getBytes());
                return list;
            } catch (Exception exe){
                Toast.makeText(context, "Error Occured !", Toast.LENGTH_SHORT).show();
            }
        }
        return empty_list;
    }

    private class CaseStudy {
        String heading;
        String desc;
        public CaseStudy(String heading, String desc){
            this.heading = heading;
            this.desc = desc;
        }
    }

    private class ListAdapter extends ArrayAdapter<CaseStudy>{

        List<CaseStudy> list = new ArrayList<>();

        public ListAdapter(Context context, List<CaseStudy> list) {
            super(context, R.layout.case_studies_card_item, list);
            this.list = list;
        }

        @Override
        public View getView(int position, View convertView, ViewGroup parent) {
            View rootView = inflater.inflate(R.layout.case_studies_card_item, parent, false);

            ((TextView)rootView.findViewById(R.id.heading)).setText(list.get(position).heading);
            ((TextView)rootView.findViewById(R.id.desc)).setText(list.get(position).desc);

            return rootView;
        }
    }

}
