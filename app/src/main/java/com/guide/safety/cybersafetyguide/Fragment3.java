package com.guide.safety.cybersafetyguide;

import android.content.Context;
import android.graphics.Color;
import android.os.Bundle;
import android.os.Handler;
import android.support.v4.app.Fragment;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.widget.CardView;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;
import android.widget.Toast;

import org.json.JSONArray;
import org.json.JSONObject;

import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.InputStream;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by root on 10/6/16.
 */
public class Fragment3 extends Fragment {
    private static Context context;

    public Fragment3(){

    }

    public void addContext(Context context){
        this.context = context;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        final View rootView = inflater.inflate(R.layout.guideline_fragment_3, container, false);

        final RecyclerView rv = (RecyclerView) rootView.findViewById(R.id.rv_recycler_view);
        rv.setHasFixedSize(true);
        MyAdapter adapter = new MyAdapter(getJson("json_guideline_corporate.txt"));
        rv.setAdapter(adapter);

        ((SwipeRefreshLayout)rootView.findViewById(R.id.guideline_refresh_layout_3)).setColorSchemeColors(Color.BLUE, Color.RED, Color.YELLOW, Color.GREEN);
        ((SwipeRefreshLayout)rootView.findViewById(R.id.guideline_refresh_layout_3)).setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                new Thread(new Runnable() {
                    @Override
                    public void run() {
                        final List<String> list = new ArrayList<>();
                        boolean done = false;
                        try {
                            URL url = new URL("http://www.abboniss.com/test/guideline_corporate_get.php");
                            HttpURLConnection urlConnection = (HttpURLConnection) url.openConnection();
                            InputStream in = urlConnection.getInputStream();
                            String jsonText = "";
                            int c;
                            while ((c=in.read()) != -1){
                                jsonText += (char)c;
                            }
                            FileOutputStream file = context.openFileOutput("json_guideline_corporate.txt", Context.MODE_PRIVATE);
                            file.write(jsonText.getBytes());
                            JSONObject jsonObject = new JSONObject(jsonText);
                            JSONArray data = jsonObject.getJSONArray("info");
                            for (int i=0; i<data.length(); i++){
                                list.add(String.valueOf(data.get(i)));
                            }
                            done = true;
                        } catch (Exception e){
                            done = false;
                        }
                        final boolean finaldone = done;
                        new Handler(context.getMainLooper()).post(new Runnable() {
                            @Override
                            public void run() {
                                if (finaldone){
                                    MyAdapter adapter = new MyAdapter(list);
                                    rv.setAdapter(adapter);
                                } else {
                                    Toast.makeText(context, "Unable to refresh", Toast.LENGTH_SHORT).show();
                                }
                                ((SwipeRefreshLayout)rootView.findViewById(R.id.guideline_refresh_layout_3)).setRefreshing(false);
                            }
                        });
                    }
                }).start();
            }
        });

        LinearLayoutManager llm = new LinearLayoutManager(getActivity());
        rv.setLayoutManager(llm);

        return rootView;
    }

    private List<String> getJson(String fileName){
        List<String> emptyList = new ArrayList<>();
        try {
            List<String> list = new ArrayList<>();
            FileInputStream file = context.openFileInput(fileName);
            String JsonText = "";
            int c;
            while ((c = file.read()) != -1){
                JsonText += (char)c;
            }
            JSONObject jsonObject = new JSONObject(JsonText);
            JSONArray data = jsonObject.getJSONArray("info");
            for (int i=0; i<data.length(); i++){
                list.add(String.valueOf(data.get(i)));
            }
            return list;
        } catch (Exception e){
            // some default json encoded data
            List<String> list = new ArrayList<>();
            String JsonText = "{\"info\":[\"Use strong passwords. Choose passwords that are difficult or impossible to guess. Give different passwords to all other accounts.\",\"Make regular back-up of critical data. Back-up must be made atleast once in each day. Larger organizations should perform a full back-up weekly and incremental back-up every day. Atleast once in a month the back-up media should be verified.\",\"Use virus protection software. That means three things: having it on your computer in the first place, checking daily for new virus signature updates, and then actually scanning all the files on your computer periodically.\",\"Use a firewall as a gatekeeper between your computer and the Internet. Firewalls are usually software products. They are essential for those who keep their computers online through the popular DSL and cable modem connections but they are also valuable for those who still dial in.\",\"Do not keep computers online when not in use. Either shut them off or physically disconnect them from Internet connection.\",\"Do not open e-mail attachments from strangers, regardless of how enticing the subject line or attachment may be.Be suspicious of any unexpected e-mail attachment from someone you do know because it may have been sent without that person's knowledge from an infected machine.\",\"Regularly download security patches from your software vendors.\",\"Be careful about cyber laws and copyright issues try to give credit or reference of website from where you are quoting it. \",\"Business people will see as part of hiring process to know about everyone views and interests. However bad people over internet will use these sites to collect the personal information and may misuse them. \",\"Remember do not put anything personal like sensitive information about your family details, addresses, personal photographs they might be misused. \",\"You can also set the privacy settings of your account according to whom you want to allow seeing your information. \",\"Establish a clear Internet usage policy and rules for using email safely\",\"Establish a strong social media policy\"]}";
            try {
                JSONObject jsonObject = new JSONObject(JsonText);
                JSONArray data = jsonObject.getJSONArray("info");
                for (int i=0; i<data.length(); i++){
                    list.add(String.valueOf(data.get(i)));
                }
                FileOutputStream file = context.openFileOutput(fileName, context.MODE_PRIVATE);
                file.write(JsonText.getBytes());
                return list;
            } catch (Exception ex){
                Toast.makeText(context, "Error Occured !", Toast.LENGTH_SHORT).show();
            }
        }
        return emptyList;
    }

    public class MyAdapter extends RecyclerView.Adapter<MyAdapter.MyViewHolder> {
        private List<String> data = new ArrayList<>();

        // Provide a reference to the views for each data item
        // Complex data items may need more than one view per item, and
        // you provide access to all the views for a data item in a view holder
        public class MyViewHolder extends RecyclerView.ViewHolder {
            public CardView mCardView;
            public TextView mTextView;
            public MyViewHolder(View v) {
                super(v);

                mCardView = (CardView) v.findViewById(R.id.card_view);
                mTextView = (TextView) v.findViewById(R.id.tv_text);
            }
        }

        // Provide a suitable constructor (depends on the kind of dataset)
        public MyAdapter(List<String> data) {
            this.data = data;
        }

        // Create new views (invoked by the layout manager)
        @Override
        public MyAdapter.MyViewHolder onCreateViewHolder(ViewGroup parent,
                                                         int viewType) {
            // create a new view
            View v = LayoutInflater.from(parent.getContext())
                    .inflate(R.layout.guideline_card_item, parent, false);
            // set the view's size, margins, paddings and layout parameters
            MyViewHolder vh = new MyViewHolder(v);
            return vh;
        }

        @Override
        public void onBindViewHolder(MyViewHolder holder, int position) {
            holder.mTextView.setText(data.get(position));
        }


        @Override
        public int getItemCount() {
            return data.size();
        }
    }
}
