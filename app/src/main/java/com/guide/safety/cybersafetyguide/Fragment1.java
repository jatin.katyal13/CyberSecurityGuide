package com.guide.safety.cybersafetyguide;

import android.content.Context;
import android.graphics.Color;
import android.os.Bundle;
import android.os.Handler;
import android.support.v4.app.Fragment;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;
import android.support.v7.widget.CardView;
import android.widget.Toast;

import org.json.JSONArray;
import org.json.JSONObject;

import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.InputStream;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.ArrayList;
import java.util.List;

public class Fragment1 extends Fragment {

    static private Context context;

    public Fragment1() {
        // Required empty public constructor
    }

    public void addContext(Context context){
        this.context = context;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        final View rootView = inflater.inflate(R.layout.guideline_fragment_1, container, false);

        final RecyclerView rv = (RecyclerView) rootView.findViewById(R.id.rv_recycler_view);
        rv.setHasFixedSize(true);
        MyAdapter adapter = new MyAdapter(getJson("json_guideline_kids.txt"));
        rv.setAdapter(adapter);

        ((SwipeRefreshLayout)rootView.findViewById(R.id.guideline_refresh_layout_1)).setColorSchemeColors(Color.BLUE, Color.RED, Color.YELLOW, Color.GREEN);
        ((SwipeRefreshLayout)rootView.findViewById(R.id.guideline_refresh_layout_1)).setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                new Thread(new Runnable() {
                    @Override
                    public void run() {
                        final List<String> list = new ArrayList<>();
                        boolean done = false;
                        try {
                            URL url = new URL("http://www.abboniss.com/test/guideline_kids_get.php");
                            HttpURLConnection urlConnection = (HttpURLConnection) url.openConnection();
                            InputStream in = urlConnection.getInputStream();
                            String jsonText = "";
                            int c;
                            while ((c=in.read()) != -1){
                                jsonText += (char)c;
                            }
                            FileOutputStream out = context.openFileOutput("json_guideline_kids.txt", Context.MODE_PRIVATE);
                            out.write(jsonText.getBytes());
                            JSONObject jsonObject = new JSONObject(jsonText);
                            JSONArray data = jsonObject.getJSONArray("info");
                            for (int i=0; i<data.length(); i++){
                                list.add(data.getString(i));
                            }
                            done = true;
                        } catch (Exception e){
                            final Exception ex = e;
                            new Handler(context.getMainLooper()).post(new Runnable() {
                                @Override
                                public void run() {
                                    Toast.makeText(context, ex.toString(), Toast.LENGTH_SHORT).show();
                                }
                            });
                            done = false;
                        }
                        final boolean finaldone = done;
                        new Handler(context.getMainLooper()).post(new Runnable() {
                            @Override
                            public void run() {
                                if (finaldone){
                                    MyAdapter adapter = new MyAdapter(list);
                                    rv.setAdapter(adapter);
                                } else {
                                    Toast.makeText(context, "Unable to refresh", Toast.LENGTH_SHORT).show();
                                }
                                ((SwipeRefreshLayout)rootView.findViewById(R.id.guideline_refresh_layout_1)).setRefreshing(false);
                            }
                        });
                    }
                }).start();
            }
        });

        LinearLayoutManager llm = new LinearLayoutManager(getActivity());
        rv.setLayoutManager(llm);

        return rootView;
    }

    private List<String> getJson(String fileName){
        List<String> emptyList = new ArrayList<>();
        try {
            List<String> list = new ArrayList<>();
            FileInputStream file = context.openFileInput(fileName);
            String JsonText = "";
            int c;
            while ((c = file.read()) != -1){
                JsonText += (char)c;
            }
            JSONObject jsonObject = new JSONObject(JsonText);
            JSONArray data = jsonObject.getJSONArray("info");
            for (int i=0; i<data.length(); i++){
                list.add(String.valueOf(data.get(i)));
            }
            return list;
        } catch (Exception e){
            // some default json encoded data
            List<String> list = new ArrayList<>();
            String JsonText = "{\"info\":[\"Do not give optional information while creating a profile, you do not need to enter all of the information that is requested , fill only mandatory information . \",\"Accept request of only people you know and trust is a great way to ensure safety when using social networking sites. \",\"'Think Before You Post'-Share the information which is required ? Always think will the information you share put yourself or someone else in danger? \",\"Do not share about your exact location on social networking site. \",\"Before sharing information to the public, always check does your post give out too much personal information of yours or not ? \",\"If you want to meet a social networking friends in person, Think before you meet, it may not be true identity posted on a web site. If you are going to meet then inform to your parents and do it in a public place during the day. \",\"Use e-mail filtering software to avoid spam so that only messages from authorized users are received. Most e-Mail providers offer filtering services.\",\"Avoid filling forms that come via e-Mail asking for your personal information. And do not click on links that come via e-Mail. \",\"Always give respect to the content you copy from different site and mention their reference while quoting it anywhere, you have to be careful about the copyright act. \",\"Do not give out identifying information such as Name, Home address, School Name or Telephone Number in a chat room. \",\"Always follow netiquettes while on social website. \",\"Be careful while sharing or passing any message to your friends , check the authenticity before you forward it. \",\"Do not respond to messages or bulletin board items that are suggestive, obscene, belligerent or threatening. \",\"Third level of privacy i.e. `Friends Only` . Always choose this option to maintain your privacy settings to choose from for your profile. \",\"Always remember one line 'NOTHING COMES AS FREE' so be careful on popup messages like 'you won' or 'you will get free', just ignore the message. \"]}";
            try {
                JSONObject jsonObject = new JSONObject(JsonText);
                JSONArray data = jsonObject.getJSONArray("info");
                for (int i=0; i<data.length(); i++){
                    list.add(String.valueOf(data.get(i)));
                }
                FileOutputStream file = context.openFileOutput(fileName, context.MODE_PRIVATE);
                file.write(JsonText.getBytes());
                return list;
            } catch (Exception ex){
                Toast.makeText(context, "Error Occured !", Toast.LENGTH_SHORT).show();
            }
        }
        return emptyList;
    }


    public class MyAdapter extends RecyclerView.Adapter<MyAdapter.MyViewHolder> {
        private List<String> data = new ArrayList<>();

        // Provide a reference to the views for each data item
        // Complex data items may need more than one view per item, and
        // you provide access to all the views for a data item in a view holder
        public class MyViewHolder extends RecyclerView.ViewHolder {
            public CardView mCardView;
            public TextView mTextView;
            public MyViewHolder(View v) {
                super(v);

                mCardView = (CardView) v.findViewById(R.id.card_view);
                mTextView = (TextView) v.findViewById(R.id.tv_text);
            }
        }

        // Provide a suitable constructor (depends on the kind of dataset)
        public MyAdapter(List<String> data) {
            this.data = data;
        }

        // Create new views (invoked by the layout manager)
        @Override
        public MyAdapter.MyViewHolder onCreateViewHolder(ViewGroup parent,
                                                         int viewType) {
            // create a new view
            View v = LayoutInflater.from(parent.getContext())
                    .inflate(R.layout.guideline_card_item, parent, false);
            // set the view's size, margins, paddings and layout parameters
            MyViewHolder vh = new MyViewHolder(v);
            return vh;
        }

        @Override
        public void onBindViewHolder(MyViewHolder holder, int position) {
            holder.mTextView.setText(data.get(position));
        }


        @Override
        public int getItemCount() {
            return data.size();
        }
    }

}