package com.guide.safety.cybersafetyguide;

import android.app.Activity;
import android.content.Intent;
import android.os.Looper;
import android.os.StrictMode;
import android.os.Bundle;
import android.os.Handler;
import android.util.Log;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.Timer;
import java.util.TimerTask;

public class Splash extends Activity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (android.os.Build.VERSION.SDK_INT > 9) {
            StrictMode.ThreadPolicy policy = new StrictMode.ThreadPolicy.Builder().permitAll().build();
            StrictMode.setThreadPolicy(policy);
        }
        setContentView(R.layout.activity_splash);

        final Thread thread = new Thread(new Runnable() {
            @Override
            public void run() {
                try {
                    URL url = new URL("http://www.abboniss.com/test/get_version.php");
                    HttpURLConnection connection = (HttpURLConnection)url.openConnection();
                    InputStream in = connection.getInputStream();
                    String json = "";
                    int c;
                    while ((c = in.read()) != -1){
                        json+=(char)c;
                    }
                    JSONObject object = new JSONObject(json);
                    in = openFileInput("version.txt");
                    String jsonLocal = "";
                    while ((c = in.read()) != -1){
                        jsonLocal+=(char)c;
                    }
                    JSONObject objectLocal = new JSONObject(jsonLocal);

                    if (Integer.parseInt(object.getString("about_us")) > Integer.parseInt(objectLocal.getString("about_us"))){
                        //update about_us
                        InputStream get = ((HttpURLConnection)(new URL("http://www.abboniss.com/test/about_us_get.php").openConnection())).getInputStream();
                        int a;
                        String jsonText = "";
                        while ((a = get.read()) != -1){
                            jsonText += (char)a;
                        }
                        openFileOutput("about_us.txt", MODE_PRIVATE).write(jsonText.getBytes());
                        openFileOutput("version.txt", MODE_PRIVATE).write(json.getBytes());
                        Log.w("jatin", "about us");
                    }

                    if (Integer.parseInt(object.getString("victim")) > Integer.parseInt(objectLocal.getString("victim"))){
                        //update victim
                        InputStream get = ((HttpURLConnection)(new URL("http://www.abboniss.com/test/victim_get.php").openConnection())).getInputStream();
                        int a;
                        String jsonText = "";
                        while ((a = get.read()) != -1){
                            jsonText += (char)a;
                        }
                        openFileOutput("victim.txt", MODE_PRIVATE).write(jsonText.getBytes());
                        openFileOutput("version.txt", MODE_PRIVATE).write(json.getBytes());
                        Log.w("jatin", "victim");
                    }

                    if (Integer.parseInt(object.getString("threats")) > Integer.parseInt(objectLocal.getString("threats"))){
                        //update threats
                        InputStream get = ((HttpURLConnection)(new URL("http://www.abboniss.com/test/threats_get.php").openConnection())).getInputStream();
                        int a;
                        String jsonText = "";
                        while ((a = get.read()) != -1){
                            jsonText += (char)a;
                        }
                        openFileOutput("threats.txt", MODE_PRIVATE).write(jsonText.getBytes());
                        openFileOutput("version.txt", MODE_PRIVATE).write(json.getBytes());
                        Log.w("jatin", "threats");
                    }

                    if (Integer.parseInt(object.getString("kids")) > Integer.parseInt(objectLocal.getString("kids"))){
                        //update kids
                        InputStream get = ((HttpURLConnection)(new URL("http://www.abboniss.com/test/guideline_kids_get.php").openConnection())).getInputStream();
                        int a;
                        String jsonText = "";
                        while ((a = get.read()) != -1){
                            jsonText += (char)a;
                        }
                        openFileOutput("json_guideline_kids.txt", MODE_PRIVATE).write(jsonText.getBytes());
                        openFileOutput("version.txt", MODE_PRIVATE).write(json.getBytes());
                        Log.w("jatin", "kids");
                    }

                    if (Integer.parseInt(object.getString("parents")) > Integer.parseInt(objectLocal.getString("parents"))){
                        //update parents
                        InputStream get = ((HttpURLConnection)(new URL("http://www.abboniss.com/test/guideline_parents_get.php").openConnection())).getInputStream();
                        int a;
                        String jsonText = "";
                        while ((a = get.read()) != -1){
                            jsonText += (char)a;
                        }
                        openFileOutput("json_guideline_parents.txt", MODE_PRIVATE).write(jsonText.getBytes());
                        openFileOutput("version.txt", MODE_PRIVATE).write(json.getBytes());
                        Log.w("jatin", "parents");
                    }

                    if (Integer.parseInt(object.getString("corporates")) > Integer.parseInt(objectLocal.getString("corporates"))){
                        //update corporates
                        InputStream get = ((HttpURLConnection)(new URL("http://www.abboniss.com/test/guideline_corporate_get.php").openConnection())).getInputStream();
                        int a;
                        String jsonText = "";
                        while ((a = get.read()) != -1){
                            jsonText += (char)a;
                        }
                        openFileOutput("json_guideline_corporate.txt", MODE_PRIVATE).write(jsonText.getBytes());
                        openFileOutput("version.txt", MODE_PRIVATE).write(json.getBytes());
                        Log.w("jatin", "corporate");
                    }

                    if (Integer.parseInt(object.getString("cyber_cells")) > Integer.parseInt(objectLocal.getString("cyber_cells"))){
                        //update cyber_cells
                        InputStream get = ((HttpURLConnection)(new URL("http://www.abboniss.com/test/cyber_cells_get.php").openConnection())).getInputStream();
                        int a;
                        String jsonText = "";
                        while ((a = get.read()) != -1){
                            jsonText += (char)a;
                        }
                        openFileOutput("cyber_cells.txt", MODE_PRIVATE).write(jsonText.getBytes());
                        openFileOutput("version.txt", MODE_PRIVATE).write(json.getBytes());
                        Log.w("jatin", "cyber_cells");
                    }

                    if (Integer.parseInt(object.getString("dodont")) > Integer.parseInt(objectLocal.getString("dodont"))){
                        //update dodont
                        InputStream get = ((HttpURLConnection)(new URL("http://www.abboniss.com/test/dodont_get.php").openConnection())).getInputStream();
                        int a;
                        String jsonText = "";
                        while ((a = get.read()) != -1){
                            jsonText += (char)a;
                        }
                        openFileOutput("json_dodont.txt", MODE_PRIVATE).write(jsonText.getBytes());
                        openFileOutput("version.txt", MODE_PRIVATE).write(json.getBytes());
                        Log.w("jatin", "dodont");
                    }

                } catch (Exception e){

                }



                final Intent main = new Intent(Splash.this, MainActivity.class);
                Intent first = new Intent(Splash.this, welcome.class);
                try {
                    InputStream in = openFileInput("secure.db");
                    String json = "";
                    int c;
                    while ((c = in.read()) != -1){
                        json += (char)c;
                    }
                    JSONObject object = new JSONObject(json);
                    String name = object.getString("name");
                    String user = object.getString("email");
                    String pass = object.getString("pass");

                    HttpURLConnection connection = (HttpURLConnection)((new URL("http://www.abboniss.com/test/isAuthentic.php?code=securepass@csao2016&user="+user+"&pass="+pass)).openConnection());
                    InputStream res = connection.getInputStream();
                    String message = "";
                    while ((c = res.read()) != -1){
                        message += (char)c;
                    }

                    if (message.equals("true")){
                        main.putExtra("name", name);
                        main.putExtra("email", user);
                        startActivity(main);
                        Splash.this.finish();
                    } else {
                        startActivity(first);
                        Splash.this.finish();
                    }

                } catch (FileNotFoundException e){
                    //exception in reading file
                    startActivity(first);
                    Splash.this.finish();

                } catch (IOException e) {
                    //exception in internet
                    try {
                        InputStream in = openFileInput("secure.db");
                        String json = "";
                        int c;
                        while ((c = in.read()) != -1){
                            json += (char)c;
                        }
                        JSONObject object = new JSONObject(json);
                        String name = object.getString("name");
                        String user = object.getString("email");
                        String pass = object.getString("pass");
                        main.putExtra("name", name);
                        main.putExtra("email", user);

                        Timer t = new Timer();
                        t.schedule(new TimerTask() {
                            @Override
                            public void run() {
                                startActivity(main);
                                Splash.this.finish();
                            }
                        }, 1000);
                    } catch (Exception ex){
                        startActivity(first);
                        Splash.this.finish();
                    }

                } catch (JSONException e) {
                    //decoding error , corrupt secure.db file
                    startActivity(first);
                    Splash.this.finish();
                }
            }
        });

        thread.start();
        Timer timer = new Timer();
        timer.schedule(new TimerTask() {
            @Override
            public void run() {
               thread.interrupt();
            }
        }, 3000);

    }
}
