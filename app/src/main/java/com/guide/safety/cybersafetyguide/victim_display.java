package com.guide.safety.cybersafetyguide;

import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Color;
import android.os.Bundle;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.view.ContextThemeWrapper;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

import org.json.JSONObject;

import java.io.FileInputStream;
import java.net.URLEncoder;
import java.util.List;

import pl.pawelkleczkowski.customgauge.CustomGauge;

public class victim_display extends AppCompatActivity {

    Thread thread = new Thread();

    @Override
    public void onBackPressed() {
        thread.interrupt();
        super.onBackPressed();
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_victim_display);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        final Intent intent = getIntent();
        int score = intent.getIntExtra("score", 0);
        CustomGauge gauge =(CustomGauge)findViewById(R.id.gauge);
        gauge.setValue(score);
        ((TextView)findViewById(R.id.gaugeText)).setText(String.valueOf(score));

        final Button button = (Button)findViewById(R.id.send);

        assert button != null;
        button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                button.setText("Sending...");
                button.setEnabled(false);
                button.setBackgroundColor(Color.parseColor("#FF8B8888"));
                thread = new Thread(new Runnable() {
                    @Override
                    public void run() {
                        try {
                            FileInputStream file = openFileInput("secure.db");
                            String res = "";
                            int c;
                            while ((c = file.read()) != -1){
                                res += (char)c;
                            }
                            JSONObject object = new JSONObject(res);
                            String email = object.getString("email");
                            String name = object.getString("name");
                            String phone = object.getString("phone");
                            List<String> list = intent.getStringArrayListExtra("probs");
                            String body = "Hello sir i am facing the following issues\r\n\r\n";
                            for (int i =0; i<list.size(); i++){
                                body += list.get(i)+"\r\n";
                            }

                            body+= "\r\nName: " + name;
                            body+= "\r\nEmail: " + email;
                            body+= "\r\nPhone: " + phone;

                            lib.executePost("http://www.abboniss.com/test/sendMail.php",
                                        "code=" + URLEncoder.encode("securepass@csao2016", "UTF-8") +
                                        "&to=" + URLEncoder.encode("ask.csao@gmail.com", "UTF-8") +
                                        "&subject=" + URLEncoder.encode("Victim Page", "UTF-8") +
                                        "&from=" + URLEncoder.encode(email, "UTF-8") +
                                        "&body=" + URLEncoder.encode(body, "UTF-8")
                                    );

                            runOnUiThread(new Runnable() {
                                @Override
                                public void run() {
                                    AlertDialog.Builder builder = new AlertDialog.Builder(new ContextThemeWrapper(victim_display.this, R.style.myDialog));
                                    builder.setTitle("Request Sent");
                                    builder.setMessage("Our Experts will contact you within 48 hours to resolve your problem");
                                    builder.setPositiveButton("OK", new DialogInterface.OnClickListener() {
                                        @Override
                                        public void onClick(DialogInterface dialog, int which) {
                                            victim_display.this.finish();
                                        }
                                    });
                                    AlertDialog alert = builder.create();
                                    alert.show();
                                }
                            });
                        } catch (Exception e){
                            runOnUiThread(new Runnable() {
                                @Override
                                public void run() {
                                    Toast.makeText(victim_display.this, "Unable To Send", Toast.LENGTH_SHORT).show();
                                }
                            });
                        }
                        runOnUiThread(new Runnable() {
                            @Override
                            public void run() {
                                button.setText("CONTACT CYBER EXPERT");
                                button.setEnabled(true);
                                button.setBackgroundColor(Color.parseColor("#3fb568"));
                            }
                        });
                    }
                });
                thread.start();

            }
        });


    }

}
