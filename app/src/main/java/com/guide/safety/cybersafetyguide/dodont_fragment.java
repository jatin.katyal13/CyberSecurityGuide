package com.guide.safety.cybersafetyguide;


import android.content.Context;
import android.graphics.Color;
import android.os.Bundle;
import android.os.Handler;
import android.support.v4.app.Fragment;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.widget.CardView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.AlphaAnimation;
import android.view.animation.Animation;
import android.widget.ArrayAdapter;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import org.json.JSONArray;
import org.json.JSONObject;

import java.io.BufferedInputStream;
import java.io.InputStream;
import java.io.OutputStream;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.ArrayList;
import java.util.List;

public class dodont_fragment extends Fragment {

    private static Context context;
    private LayoutInflater inflater;

    public dodont_fragment() {
        // Required empty public constructor
    }

    public void add_context(Context context){
        this.context = context;
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        this.inflater = inflater;
        final View rootView = inflater.inflate(R.layout.content_dodont, container, false);

        Animation animation = new AlphaAnimation(1,5);

        ArrayAdapter<dodont> adapter = new MyAdapter(getJson());
        ListView listView = (ListView) rootView.findViewById(R.id.dodont_ListView);
        listView.setAnimation(animation);
        listView.setAdapter(adapter);

        final SwipeRefreshLayout refreshLayout = (SwipeRefreshLayout) rootView.findViewById(R.id.dodont_swipe_layout);
        refreshLayout.setColorSchemeColors(Color.BLUE, Color.RED, Color.YELLOW, Color.GREEN);
        refreshLayout.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                new Thread(){
                    @Override
                    public void run() {
                        super.run();
                        boolean done = false;
                        final List<dodont> list = new ArrayList<>();
                        try {
                            URL url = new URL("http://www.abboniss.com/test/dodont_get.php");
                            HttpURLConnection urlConnection = (HttpURLConnection) url.openConnection();
                            InputStream in = new BufferedInputStream((urlConnection.getInputStream()));
                            int c;
                            String jsonText = "";
                            while ((c = in.read()) != -1){
                                jsonText += String.valueOf((char)c);
                            }
                            OutputStream file = context.openFileOutput("json_dodont.txt", context.MODE_PRIVATE);
                            file.write(jsonText.getBytes());
                            JSONObject obj = new JSONObject(jsonText);
                            JSONArray net = obj.getJSONArray("dodont");
                            for (int i=0; i<net.length(); i++){
                                JSONObject temp = net.getJSONObject(i);
                                list.add(new dodont(temp.getString("info"), temp.getString("dos").equals("1")));
                            }
                            done = true;
                        }
                        catch (Exception e){
                            done = false;
                        }
                        final boolean finalDone = done;
                        new Handler(context.getMainLooper()).post(new Runnable() {
                            @Override
                            public void run() {
                                if (finalDone){
                                    ArrayAdapter<dodont> adapter = new MyAdapter(list);
                                    ListView listView = (ListView) rootView.findViewById(R.id.dodont_ListView);
                                    listView.setAdapter(adapter);
                                }
                                else {
                                    Toast.makeText(context, "Unable to refresh !", Toast.LENGTH_SHORT).show();
                                }
                                refreshLayout.setRefreshing(false);
                            }
                        });
                    }
                }.start();
            }
        });

        return rootView;
    }

    private class MyAdapter extends ArrayAdapter<dodont>{
        List<dodont> list = new ArrayList<>();
        MyAdapter(List<dodont> list){
            super(context, R.layout.dodont_card_item, list);
            this.list = list;
        }
        @Override
        public View getView(int position, View view, ViewGroup viewgroup){
            View itemview = view;
            if (itemview == null){
                itemview = inflater.inflate(R.layout.dodont_card_item, viewgroup, false);
            }

            TextView main = (TextView)itemview.findViewById(R.id.tv_text);
            main.setText(list.get(position).info);
            if (list.get(position).dos){
                ((CardView)itemview.findViewById(R.id.card_view)).setCardBackgroundColor(Color.parseColor("#a5d6a7"));
            } else {
                ((CardView)itemview.findViewById(R.id.card_view)).setCardBackgroundColor(Color.parseColor("#ffccbc"));
            }

            return itemview;
        }
    }

    private class dodont {
        public String info;
        public boolean dos;

        dodont (String info, boolean dos){
            this.info = info;
            this.dos = dos;
        }
    }

    private List<dodont> getJson(){
        List<dodont> empty_list = new ArrayList<>();
        try {
            List<dodont> list = new ArrayList<>();
            InputStream file = context.openFileInput("json_dodont.txt");
            String jsonText = "";
            int c;
            while ((c = file.read()) != -1){
                jsonText += String.valueOf((char)c);
            }
            JSONObject obj = new JSONObject(jsonText);
            JSONArray net = obj.getJSONArray("dodont");
            for (int i=0; i<net.length(); i++){
                JSONObject temp = net.getJSONObject(i);
                list.add(new dodont(temp.getString("info"), temp.getString("dos").equals("1")));
            }
            return list;
        } catch (Exception ex){
            try {
                List<dodont> list = new ArrayList<>();
                String jsonText = "{\"dodont\":[{\"info\":\"Save all communications for evidence. Do not edit or alter them in any way. Also, keep a record of your contacts with Internet System Administrators or Law Enforcement Officials. \", \"dos\":\"1\"},{\"info\":\"Set your browser security to high and add safe websites to trusted website zone.\", \"dos\":\"0\"},{\"info\":\"Disable the login and remembering passwords information. \", \"dos\":\"0\"},{\"info\":\"Block pop up window in your web browser \", \"dos\":\"0\"},{\"info\":\"Enable the option “warn me when sites try to install extensions/theme” in your web browser.\", \"dos\":\"0\"},{\"info\":\"Try to have separate email id other than your official id to be used in social website. \", \"dos\":\"0\"},{\"info\":\"Be extremely careful about how you share personal information about yourself online.\", \"dos\":\"0\"},{\"info\":\"Choose your chatting nickname carefully so as not to offend others.\", \"dos\":\"0\"},{\"info\":\"Be extremely cautious about meeting online acquaintances in person. If you choose to meet, do so in a public place and take along a friend.\", \"dos\":\"0\"},{\"info\":\"Remember that all other internet users are strangers. You do not know who you are chatting with. So be careful and polite.\", \"dos\":\"0\"},{\"info\":\"If a situation online becomes hostile, log off or surf elsewhere. If a situation places you in fear, contact a local law enforcement agency.\", \"dos\":\"0\"},{\"info\":\"Make sure that your ISP and Internet Relay Chart (IRC) network have an acceptable use policy that prohibits cyber-stalking.\", \"dos\":\"0\"},{\"info\":\"Always install recommended updates and install original Do Use antivirus and anti spyware programs.\", \"dos\":\"0\"},{\"info\":\"Internet is the unsafe place. Be smart about making yourself and your systems safer.\", \"dos\":\"0\"},{\"info\":\"Donot delete harmful communications (emails, chat logs, posts etc). These may help provide vital information about the identity of the person behind these. \", \"dos\":\"1\"},{\"info\":\"Do not share personal information in public spaces anywhere online, do not give it to strangers, including in e-mail or chat rooms.\", \"dos\":\"1\"},{\"info\":\"Do not keep your email id or other logins open when not in use. \", \"dos\":\"1\"},{\"info\":\"Do not open e-mail attachments from strangers, regardless of how enticing the subject line or attachment may be.\", \"dos\":\"1\"},{\"info\":\"Be suspicious of any unexpected e-mail attachment from someone you do know because it may have been sent without that person’s knowledge from an infected machine.\", \"dos\":\"1\"},{\"info\":\" Do not give your password to anybody. Somebody who is malicious can cause great harm to you and your reputation. It is like leaving your house open for a stranger and walking away.\", \"dos\":\"1\"},{\"info\":\"Do not copy a program that is copyrighted on the net. It is illegal. \", \"dos\":\"1\"},{\"info\":\"When talking to somebody new on the net, do not give away personal information-like numbers of the credit card used by your parents, your home addresses/ phone numbers and such other personal information. \", \"dos\":\"1\"}]}";
                JSONObject obj = new JSONObject(jsonText);
                JSONArray net = obj.getJSONArray("dodont");
                for (int i = 0; i < net.length(); i++) {
                    JSONObject temp = net.getJSONObject(i);
                    list.add(new dodont(temp.getString("info"), temp.getString("dos").equals("1")));
                }
                OutputStream out = context.openFileOutput("json_dodont.txt", context.MODE_PRIVATE);
                out.write(jsonText.getBytes());
                return list;
            } catch (Exception exe){
                Toast.makeText(context, "Error Occured !", Toast.LENGTH_SHORT).show();
            }
        }
        return empty_list;
    }

}
