package com.guide.safety.cybersafetyguide;

import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.os.Bundle;
import android.os.StrictMode;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import org.json.JSONArray;
import org.json.JSONObject;

import java.io.BufferedInputStream;
import java.io.FileOutputStream;
import java.io.InputStream;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.ArrayList;
import java.util.List;

public class cyber_cells extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (android.os.Build.VERSION.SDK_INT > 9) {
            StrictMode.ThreadPolicy policy = new StrictMode.ThreadPolicy.Builder().permitAll().build();
            StrictMode.setThreadPolicy(policy);
        }
        setContentView(R.layout.activity_cyber_cells);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        final ListView listView = (ListView)findViewById(R.id.cyber_cell_list);

        final SwipeRefreshLayout refreshLayout = (SwipeRefreshLayout)findViewById(R.id.refresh_layout);

        new Thread(new Runnable() {
            @Override
            public void run() {
                runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        refreshLayout.setRefreshing(true);
                    }
                });
                final List<state_list> list = getJson();
                runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        listView.setAdapter(new MyAdapter(getApplicationContext(), list));
                        refreshLayout.setRefreshing(false);
                    }
                });
            }
        }).start();

        refreshLayout.setColorSchemeColors(Color.BLUE, Color.RED, Color.YELLOW, Color.GREEN);

        refreshLayout.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                new Thread(){
                    @Override
                    public void run() {
                        super.run();
                        final List<state_list> list = new ArrayList<>();
                        List<String> states = new ArrayList<>();
                        boolean done =false;
                        try {
                            URL url = new URL("http://abboniss.com/test/cyber_cells_get.php");
                            HttpURLConnection urlConnection = (HttpURLConnection) url.openConnection();
                            InputStream in = new BufferedInputStream((urlConnection.getInputStream()));
                            int c;
                            String jsonText = "";
                            while ((c = in.read()) != -1) {
                                jsonText += String.valueOf((char) c);
                            }
                            JSONObject obj = new JSONObject(jsonText);
                            FileOutputStream out = openFileOutput("cyber_cells.txt", MODE_PRIVATE);
                            out.write(jsonText.getBytes());

                            JSONArray state = obj.getJSONArray("states");
                            for (int i=0; i<state.length(); i++){
                                states.add(state.get(i).toString());
                            }

                            JSONObject info = obj.getJSONObject("info");
                            for (String s : states){
                                JSONArray st = info.getJSONArray(s);
                                List<Branch> branch = new ArrayList<>();
                                for (int i=0; i<st.length(); i++){
                                    JSONObject temp = st.getJSONObject(i);
                                    branch.add(new Branch(temp.getString("name"), temp.getString("desig"), temp.getString("number"), temp.getString("email")));
                                }
                                list.add(new state_list(s, branch));
                            }

                            done = true;
                        }
                        catch (Exception e){
                            done = false;
                        }

                        final boolean finalDone = done;
                        runOnUiThread(new Runnable() {
                            @Override
                            public void run() {
                                if (finalDone) {
                                    ((ListView)findViewById(R.id.cyber_cell_list)).setAdapter(new MyAdapter(getApplicationContext(), list));
                                }
                                else {
                                    Toast.makeText(cyber_cells.this, "Unable to refresh !", Toast.LENGTH_SHORT).show();
                                }
                                refreshLayout.setRefreshing(false);
                            }
                        });
                    }
                }.start();
            }
        });

        listView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                Intent intent = new Intent(getApplicationContext(), cyber_cells_display.class);
                List<state_list> list = getJson();
                intent.putExtra("state", list.get(position).state);
                intent.putExtra("name_list", (ArrayList<String>)list.get(position).get_name_list());
                intent.putExtra("desig_list", (ArrayList<String>)list.get(position).get_desig_list());
                intent.putExtra("number_list", (ArrayList<String>)list.get(position).get_number_list());
                intent.putExtra("email_list", (ArrayList<String>)list.get(position).get_email_list());
                startActivity(intent);
            }
        });
    }



    private List<state_list> getJson(){
            try {
                final List<state_list> list = new ArrayList<>();
                List<String> states = new ArrayList<>();
                InputStream in = openFileInput("cyber_cells.txt");
                int c;
                String jsonText = "";
                while ((c = in.read()) != -1) {
                    jsonText += String.valueOf((char) c);
                }
                JSONObject obj = new JSONObject(jsonText);
                FileOutputStream out = openFileOutput("cyber_cells.txt", MODE_PRIVATE);
                out.write(jsonText.getBytes());

                JSONArray state = obj.getJSONArray("states");
                for (int i=0; i<state.length(); i++){
                    states.add(state.get(i).toString());
                }

                JSONObject info = obj.getJSONObject("info");
                for (String s : states){
                    JSONArray st = info.getJSONArray(s);
                    List<Branch> branch = new ArrayList<>();
                    for (int i=0; i<st.length(); i++){
                        JSONObject temp = st.getJSONObject(i);
                        branch.add(new Branch(temp.getString("name"), temp.getString("desig"), temp.getString("number"), temp.getString("email")));
                    }
                    list.add(new state_list(s, branch));
                }
                return list;
            } catch (Exception ex){
                try {
                    final List<state_list> list = new ArrayList<>();
                    List<String> states = new ArrayList<>();
                    String jsonText = "{\"states\":[\"Mumbai\",\"Arunachal Pradesh\",\"Andhra Pradesh\",\"Gujarat\",\"Haryana\",\"Himachal Pradesh\",\"Jammu&Kashmir\",\"Jharkhand\",\"Chandigarh\",\"New Delhi\",\"UttaraKhand\",\"Uttar Pradesh\",\"Tripura\",\"Manipur\",\"Madhya Pradesh\",\"Meghalaya\",\"Orissa\",\"Rajasthan\",\"Tamil Nadu\",\"Karnataka\",\"West Bengal\",\"Maharashtra\"],\"info\":{\"Mumbai\":[{\"name\":\"Cyber Crime Investigation Cell\", \"desig\": \"Superintendent of Police, Cyber Crime Investigation Cell, Central Bureau of Investigation, 5th Flo\", \"number\": \"1124361271\", \"email\":\"Speou9del@cbi.gov.in\"}],\"Arunachal Pradesh\":[{\"name\":\"CID HQ\", \"desig\": \"Additional DGP,CID Ulubari, Guwahati-781007\", \"number\": \"2521618\", \"email\":\"Ssp_cid@assampolice.com\"}],\"Andhra Pradesh\":[{\"name\":\"Cyber Crime Police Station, Hyderabad City Police\", \"desig\": \"ACP,Cyber Crime Police Station, Beside Control Room,OPP Kalanjali, Beside L.B.Stadium ,Hyderabad\", \"number\": \"2785040\", \"email\":\"Cybercell_hyd@hyd.appolice.gov.in\"},{\"name\":\"Cyber Crime Police Station, Cyberabad City Police\", \"desig\": \"Police Commissioner's Office Old Mumbai Road,Jayabheri Pine Valley,Gachibowli Hyderabad ,Andhra Pr\", \"number\": \"27854031\", \"email\":\"Sho_cybercrime@cyb.appolice.gov.in\"},{\"name\":\"Cyber Crime Police Station,CID,Hyderabad\", \"desig\": \"1/C Cyber Cell, 3rd Floor, Crimes Investigation Department, A C,Guards,Hyderabad\", \"number\": \"23307256\", \"email\":\"cybercrimesps@cid.appolice.gov.in\"},{\"name\":\"Cyber Crime Investigation Unit(CCIU)\", \"desig\": \"Dy.S.P,Kotwali police station, Patna \", \"number\": \"9431818398\", \"email\":\"Cciu-bih@nic.in\"}],\"Gujarat\":[{\"name\":\"State CID,Crime & Rly,Gujarat State\", \"desig\": \"DIG,CID Crime,4th Floor, Police Bhavan,Sector-18,Gandhinagar\", \"number\": \"3250798\", \"email\":\"cc-cid@gujarat.gov.in\"},{\"name\":\"Deputy Commissioner of Police, Crime\", \"desig\": \"Gaikwad Haveli, Jamalpur, Ahmedabad\", \"number\": \"25330170\", \"email\":\"Dcp-crime-ahd@gujarat.gov.in\"}],\"Haryana\":[{\"name\":\"Cyber Crime and Technical Investigation Cell, Gurgaon\", \"desig\": \"Old S.P.Office complex, Civil Lines, Gurgaon\", \"number\": \"2329988\", \"email\":\"Jtcp.ggn@hry.nic.in\"}],\"Himachal Pradesh\":[{\"name\":\"CID HeadQuaters\", \"desig\": \"Dy,SPCID Head Quarter,Kusumpati,Shimla-9, Himachal Pradesh\", \"number\": \"9418039449\", \"email\":\"Soodbrijesh9@gmail.com\"}],\"Jammu&Kashmir\":[{\"name\":\"SSP,Crime\", \"desig\": \"CPO Complex,panjtirthi,Jammu-180004\", \"number\": \"2578901\", \"email\":\"Sspcrmjmu.jk@nic.in\"}],\"Jharkhand\":[{\"name\":\"CID,Organized Cime\", \"desig\": \"IG,CID,Rajarani Vuilding,Doranda Ranchi,834002\", \"number\": \"2444703\", \"email\":\"agupta@jharkhandpolice.gov.in\"}],\"Chandigarh\":[{\"name\":\"Cyber Crime Cell,Chandigarh\", \"desig\": \"Inspector of Police ,Cyber Crime Cell, Crime Branch Office ,Sector-11,Chandigarh\", \"number\": \"98772281713\", \"email\":\"cybercrime-chd@nic.in\"}],\"New Delhi\":[{\"name\":\"Assistant Commissioner of Police ,Cyber Crime\", \"desig\": \"Economic Offenses Wing,Police Training School Complex Malviya Nagar, New Delhi\", \"number\": \"26515229\", \"email\":\"acp-cybercell-dl@nic.in\"}],\"UttaraKhand\":[{\"name\":\"Special Task force\", \"desig\": \"DIG,STF,PHQ,12 Subhash road,Dehradun,Uttrakhand 248001\", \"number\": \"9412370272\", \"email\":\"dgc-police-ua@nic.in\"}],\"Uttar Pradesh\":[{\"name\":\"Cyber Complaints Redressal Cell\", \"desig\": \"Agra Range 7,Kutchery Road,Baluganj, Agra-232001 (UP) ,India\", \"number\": \"9410837559\", \"email\":\"info@cybercellagra.com\"}],\"Tripura\":[{\"name\":\"SP,CID\", \"desig\": \"SP,CID,Arunthati nagar,Agartala-799003\", \"number\": \"2376963\", \"email\":\"Spcid-tri@nic.in\"}],\"Manipur\":[{\"name\":\"SP,CID Crimes branch\", \"desig\": \"SP,CID,Crimes branch, Jail Road,1st bat Manipur rifle campus,Impal ,Pin:795001 \", \"number\": \"9436027465\", \"email\":\"cid-cb@man.nic.in\"}],\"Madhya Pradesh\":[{\"name\":\"State Cyber Police\", \"desig\": \"IGP,Cyber Cell Police Radio Headquarters Campus,Bhadadhadaa Road, Bhopal MP\", \"number\": \"2770248\", \"email\":\"mpcyberpolice@gmail.com\"}],\"Meghalaya\":[{\"name\":\"SP,SCRB\", \"desig\": \"SP,SCRB,Police Head Quarters,Secretariat Hill,Shillong-793001,Meghalaya\", \"number\": \"9863064997\", \"email\":\"Meghcid2002@yahoo.com\"}],\"Orissa\":[{\"name\":\"CID,Crime Branch\", \"desig\": \"SP Crime Branch, CID Crimes Branch Office, Buxybazzar,Cuttack,Orissa Pin-753001\", \"number\": \"9437450370\", \"email\":\"Sp1cidcb.orpol@nic.in\"}],\"Rajasthan\":[{\"name\":\"Special Operations Group\", \"desig\": \"Jhalana Mahal,Jagatpura Road,Malviya Nagar,Jaipur\", \"number\": \"1412759779\", \"email\":\"splcrimejpr@gmail.com\"}],\"Tamil Nadu\":[{\"name\":\"Cyber Crime Cell, Chennai City\", \"desig\": \"Commissioner Officer Campus Egmore,Chennai - 600008\", \"number\": \"4455498211\", \"email\":\"cyberac@rediffmail.com\"},{\"name\":\"Cyber Crime Cell,CID,Chennai\", \"desig\": \"No.3,SIDCO Electronic Complex ,1 Floor,Guindy,Chennai-32\", \"number\": \"4422502512\", \"email\":\"cbcyber@nic.in\"}],\"Karnataka\":[{\"name\":\"Cyber Crime Police Station\", \"desig\": \"S.P.Cyber Crime police Station, C.I.D. Headquarters, Carlton House ,1, Palace Road,Bangalore-5600\", \"number\": \"9497990330\", \"email\":\"cyberps@keralapolice.gov.in\"},{\"name\":\"Hi-Tech Crime Enquiry Cell\", \"desig\": \"Police Head Quarters, Trivandrum\", \"number\": \"4712721547\", \"email\":\"hotechcell@keralapolice.gov.in\"},{\"name\":\"Cyber Cell\", \"desig\": \"Sub-Inspector of Police, Office of the Inspector General of Police Ernakulam Range, Ernakulam\", \"number\": \"94979760045\", \"email\":\"sicybercell@kochicitypolice.org\"}],\"West Bengal\":[{\"name\":\"CID,Cyber Crime, West Bengal\", \"desig\": \"CID Cyber Crime Cell,Bhabani Bhaban\", \"number\": \"3324506163\", \"email\":\"occyber@cidwestbengal.gov.in\"},{\"name\":\"Kolkata Police Cyber Crime Police Station\", \"desig\": \"ACP,Cyber Crime Police Station,Lal Bazzar,kolkata\", \"number\": \"3322141420\", \"email\":\"cyberps@kolkatapolice.gov.in\"},{\"name\":\"North 24 Parganas Dist. Cyber Crime Cell\", \"desig\": \"S.I Cyber Crime Cell,1st Floor ,of Bidhannagar North Police Station ,Beside Tank No. 6,Saltlake ,Kolk\", \"number\": \"9836272121\", \"email\":\"\"}],\"Maharashtra\":[{\"name\":\"Cyber Crime Investigation Cell, Pune City Police\", \"desig\": \"Dy.Commissioner of Police,EOW & Cyber ,Office of the commissioner of Police, 2, Sadhu Vawani Road,Ca\", \"number\": \"2026123346\", \"email\":\"Crimecyber.pune@nic.in\"},{\"name\":\"Cyber crime Investigation Cell,Pune City Police\", \"desig\": \"Assistant Commissioner of Police, CCPS,BKC Police Station Complex, Mumbai\", \"number\": \"2226504008\", \"email\":\"Cybercell.mumbai@mahapolice.gov.in\"},{\"name\":\"Cyber Crime Cell, Thane City Police\", \"desig\": \"Cybercrime Investigation Cell, 3rd Floor ,OPP Thane Police School, Near Kharkar Lane, Thane(W)-4006\", \"number\": \"25424444\", \"email\":\"\"},{\"name\":\"Cyber Crime Cell,Nagpur City\", \"desig\": \"I/C Officer - DCP(EOW &Cyber) PSI,Cyber Crime Cell, Nagpur City, Crime Branch, New Administrative\", \"number\": \"7122566766\", \"email\":\"\"}]}}";
                    JSONObject obj = new JSONObject(jsonText);
                    FileOutputStream out = openFileOutput("cyber_cells.txt", MODE_PRIVATE);
                    out.write(jsonText.getBytes());

                    JSONArray state = obj.getJSONArray("states");
                    for (int i=0; i<state.length(); i++){
                        states.add(state.get(i).toString());
                    }

                    JSONObject info = obj.getJSONObject("info");
                    for (String s : states){
                        JSONArray st = info.getJSONArray(s);
                        List<Branch> branch = new ArrayList<>();
                        for (int i=0; i<st.length(); i++){
                            JSONObject temp = st.getJSONObject(i);
                            branch.add(new Branch(temp.getString("name"), temp.getString("desig"), temp.getString("number"), temp.getString("email")));
                        }
                        list.add(new state_list(s, branch));
                    }
                    return list;
                } catch (Exception exe){
                    runOnUiThread(new Runnable() {
                        @Override
                        public void run() {
                            Toast.makeText(cyber_cells.this, "Error Occured", Toast.LENGTH_SHORT).show();
                        }
                    });
                    return new ArrayList<>();
                }
            }
    }

    private class state_list{
        public String state;
        public List<Branch> branches;

        state_list(String state, List<Branch> branches){
            this.state = state;
            this.branches = branches;
        }

        public List<String> get_name_list(){
            List<String> list = new ArrayList<>();
            for (Branch b : branches){
                list.add(b.name);
            }
            return list;
        }

        public List<String> get_desig_list(){
            List<String> list = new ArrayList<>();
            for (Branch b : branches){
                list.add(b.desig);
            }
            return list;
        }

        public List<String> get_number_list(){
            List<String> list = new ArrayList<>();
            for (Branch b : branches){
                list.add(b.number);
            }
            return list;
        }

        public List<String> get_email_list(){
            List<String> list = new ArrayList<>();
            for (Branch b : branches){
                list.add(b.email);
            }
            return list;
        }
    }

    private class Branch {
        public String name;
        public String desig;
        public String number;
        public String email;

        Branch(String name, String desig, String number, String email){
            this.name = name;
            this.desig = desig;
            this.number = number;
            this.email = email;
        }
    }

    /*@Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.cyber_cells, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        return super.onOptionsItemSelected(item);
    }
    */
    private class MyAdapter extends ArrayAdapter<state_list>{
        List<state_list> list = new ArrayList<>();

        public MyAdapter(Context context, List<state_list> list) {
            super(context, R.layout.cyber_cell_card_item, list);
            this.list = list;
        }

        @Override
        public View getView(int position, View convertView, ViewGroup parent) {
            View rootView = convertView;
            if (rootView == null){
                rootView = getLayoutInflater().inflate(R.layout.cyber_cell_card_item, null);
            }

            ((TextView)rootView.findViewById(R.id.tv_text)).setText(list.get(position).state);

            return rootView;
        }
    }

}
